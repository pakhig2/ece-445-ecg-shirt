# ECE 445 ECG Shirt

Lab Notebook for **Pooja Bhagchandani** Team 14 (ECG Shirt) for ECE 445 Fall 2021

Note: contents were all committed at once


- [08/24/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#08242021-lecture-1)
- [08/26/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#08262021-request-for-approval)
- [08/29/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#08292021-contacting-dr-hanna-erickson)
- [08/30/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#08302021-first-meeting-with-dr-hanna-erickson-and-team)
- [09/07/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#09072021-changes-to-the-project)
- [09/08/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#09082021-pitching-project-changes-to-dr-erickson)
- [09/07/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#09/07/2021)
- [09/14/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#09142021-first-ta-meeting)
- [09/16/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#09162021-final-proposal-submission)
- [09/22/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#09222021-ta-meeting)
- [09/24/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#09242021-proposal-feedback)
- [09/27/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#09272021-updated-block-diagram)
- [09/28/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#09282021-developing-tolerance-analysis)
- [09/30/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#09302021-pcb-review-and-ddc)
- [10/05/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#10052021-design-document-and-tips-for-design-review)
- [10/06/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#10062021-design-review)
- [10/11/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#10112021-first-pcb-order)
- [10/14/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#10142021-ta-meeting-and-ml-model-progress)
- [10/21/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#10212021-ta-meeting-and-pcb)
- [10/27/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#10272021-ta-meeting-and-pcb)
- [10/31/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#1031-ipr-and-ml-model-progress)
- [11/04/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#11042021-ta-meeting-and-ml-model-updates)
- [11/10/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#11102021-testing-data-subsystem)
- [11/15/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#11152021-testing-data-subsystem)
- [11/17/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#11172021-mock-demo)
- [11/29/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#11292021-final-design)
- [12/01/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#12012021-final-pcb)
- [12/03/2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/blob/main/Pooja%20Bhagchandani/README.md#12032021-final-demo)


08/24/2021 Lecture 1
=
Today during lecture we got to see some pitched projects from different departments and professors. One that stuck out to me was the ECG Shirt, Dr. Hanna Erickson pitched a 12-lead wearable ECG shirt that is comfortable and can accurately detect one's heart beat. It can then pass the data onto a phone application that can analyze whether the person is currently at risk of a heart attack. Our request for approval is due this week so I am thinking of contacting Dr. Erickson about this project. Additionally, I have found a teammate Pakhi who is also interested in this project. 

08/26/2021 Request for Approval
=
I wrote and posted my RFA on the ECE 445 website today. I wrote the problem statement and solution that I think can work for this project, my TA Josephine responded quickly and asked for details to think about regarding:

"Making the app user friendly is an important part of this project, as well making sure the shirt accurately measure the ECG. Do you have any specific idea about the measurement system? Have you looked up how ECG electrode works and how you could replace it something less bulky?"

For this I do have a few ideas in mind that I can ask Dr. Erickson and my teammates about:
- Measurement system: filtering on board using low pass and high pass filters, cleaning ecg in software potentially
- Electrodes: ask Dr.Erickson about the potential of using flat electrodes or non stick electrodes

08/29/2021 Contacting Dr. Hanna Erickson 
= 
A few days ago I finalized with my teammates Pakhi Gupta and Ruthvik Reddy Kadiri. Pakhi and I have worked in previous classes together and we both expressed interets in the project from lecture 1. Ruthvik is also a friend who si interetsed in hardware and pcb design, this is great because pakhi and I are more into software. I think this team will work well for the three of us because we have worked together in the past and we know each others style of work.

After finalizing the team, I contacted Dr. Erickson and sent her a copy of my RFA. This is the email I sent over:

Hi Hanna! I'm Pooja Bhagchandani a senior in Computer Engineering taking ECE 445 Senior Design this semester. I have a group and we are really interested in your project, I'm the one who posted on the web board regarding my interest and I know quite a few people have contacted me wanting to hop on the project. I contacted some of the students who are interested and we have formed a team of 3. I'm very interested in the medical field regarding software and electronics, the other two students have a lot of mobile app experience and one is very strong in electronics. Here is an overview of the thread I posted with interest on our class web board: 

Problem Statement: During Tuesday's lecture Hanna talked about how heart attacks are some of the most common causes of deaths. Symptoms of a heart attack can start way before it actually happens and tracking this sort of thing would lead to a much higher chance for recovery. That is where the ECG shirt comes into play. With an ECG shirt there is no longer a use for electrodes.

Solution: The t-shirt collects any heart rhythms and transports the data so it can be viewed on a mobile application or any screen. This saves doctors a lot of time and gives them the chance of catching heart attack symptoms early on. This is also much easier than having patients come in and another feature that Hanna mentioned is that the shirt would detect any sudden dangerous changes in heart beat and accordingly alert the nearest hospital. This would save time and lives and could potentially protect others from experiencing the pain of a heart attack if caught early on. The first part would be building a mobile application and managing to connect it to the shirt. The second part would be building a prototype for this shirt and getting it to measure what we are looking for without the discomfort of electrodes. The software and hardware components of this project sound really interesting to me and I like the idea of doing something that could save lives and make an impact.

08/30/2021 First Meeting with Dr. Hanna Erickson and Team
=
We have been in contact with Dr. Erickson and finally met with her and her team today with an initial pitch. Pakhi, Ruthvik, and I each prepared some slides to present breaking her project down into 3 parts: hardware, machine learning model, and mobile application. I presented about the machine learning model and this is the slide I created. It's brief and just explains how I think we can build an ML model for the project:

![Image1](pooja_slide.png)

Her feedback on our pitch was positive, however she did say that she needed to speak with her team because other people were interested in this project. She also mentioned that she doesn't understand much about the hardware details but could help us understand details on the ECG readings and how we would need to make them clear and what our ML model needs to look for exactly in order to detect a heart attack. 

09/07/2021 Changes to the project
= 
We have been trying to get our RFA approved for a few days now but there have been some concerns about the project being too difficult to complete within 3 months. The TA's spoke to us about downsizing the project and we came up with a few potential ideas:
- I think we can decrease the number of leads, need to ask Dr. Erickson about this, could it work?
- Making the shirt machine washable and resistant to water is out of scope for the class ... pitch a ecg that can later be integrated into a shirt (suggested by TA to make a portable ECG)
- How can we confirm a heart attack is happening? Do we need all 12 leads for this? Need to ask Dr. Erickson

09/08/2021 Pitching Project Changes to Dr. Erickson
=
Today we met with Dr. Erickson and had a conversation with her about potential changes. She helped us downsize the project in a manner that it would still be useful to her and she could potentially make it 12 leads later on. She explained that using fewer leads would be okay as long as it has more than 1 (this is what an apple watch has). She explained that 3-4 leads are neecessary in order to look for heart warnings because each lead corresponds to a view of the heart. She is fine with the ECG being a "portable ECG" for now as long as it is small enough to be workied into a shirt later on.

This meeting went well, i think it will be much easier to complete this in 3 months. Not integrating it into the shirt means that noise reduction would be much easier for us. Using 3-4 leads will also be eaiser for us to program and display, my only worry is how we will look for a heart attack with such few leads. Some potential thougths I have:
- Can we make it detect only certain types of heart attacks?
- How many views of the heart will we receive?
- Should we look for heart disease? 
Overall, my worry is using 3-4 leads won't make for accurate heart attack detection, and we need to compensate for this somehow. 

09/14/2021 First TA Meeting 
=
We had our first TA Meeting with Josephine today. I had some questions about the proposal since I had began drafting the block diagaram and looking into components:
- How do we divide our system into the appropriate subsystems if our data and application are interlinked? She suggested looking into wifi/bluetooth and seperating them. 
- Looking for components: should we use a bandpass filter? High pass filter + low pass filter? Will filtering the ECG signal on hardware be enough? 
- High level requirements: How should we come up with numbers and be specific? 

She answered these questions and suggested doing research on the numbers for high level requirements. Typical speed and reasonable ECG readings usually have baseline frequencies. She also said she will give us feedback if we send in our proposal tonight, I am working on the block diagram with Pakhi and writing the application overview + tolerance analysis. We are all writing different parts so we can send it in for feedback on time. This is an image of the block diagram we have made for our draft:

![Image2](initial_block_diagram.png)

09/16/2021 Final Proposal Submission 
=
Josephine asked us to modify some things in our submission: Modify high level requirements and specify "how fast" and what is meant by "readable signal format". Overall use quantitative measurements throughout the proposal. Taking this feedback into consideration we came up with the following High level requirements and subsystem requirements:

**High Level Requirements:**  
1) The 4 leads will pick up data and transmit it to the application at a rate of 500 Hz in a
readable signal wave format that can be analyzed digitally. These leads will be the ones
placed on the: bipolar, unipolar, chest, and augmented. These 4 electrodes should be able
to pick up P, QRS, and T waves from the heart rhythm. We will use textile electrodes
rather than gel for more suited augmentation to the user.

2) The readings picked up from the 4 lead electrodes will be transmitted into a signal
waveform that can be read digitally and analyzed using ML models and programming.
The readable format for our model would be P, QRS, and T waves represented as data
points that can be viewed in a 2D graph-like format w x-axis as time and y-axis as
amplitude of the wave in mm which can also be inferred in mV. The primary purpose of
the analyzer will be to detect change in ST in comparison to a normal heartbeat.

3) Since the readings will be less accurate than an industrial standard 12 lead ECG, we will
look for a change in elevation in the ST in the inputted wave signal from the electrodes as
an indication of a potential heart attack. This will act as a preemptive warning that
something could be wrong, and the user might potentially be at a risk of a heart attack.
This warning alert will be raised by the application which will have the same refresh rate
as ECG readings (500 Hz) and will be based on the inputted readings.

**Subsystem Requirements:**  
- Requirement: Power subsystem must supply at least 1400mAh at 3.3V to the microcontroller and
at +- 2.5V to ADC’s continuously while the electrodes measure the wave and send the wave over
to our data subsystem.
- Requirement: The microcontroller in the data subsystem must collect the data from the
electrodes and supply it to the SD card at a rate of at least 500 Hz.
- Requirement: The data acquired from the subsystem should be updated on the display at a rate of
500 Hz + running time of the machine learning algorithm.

For our tolerance analysis, Josephine said it's very difficult to come up with. For the proposal, I didn't have time to come up with a quantitative measure but I have an idea for the design document. The biggest worry in our project is having our ML model be accurate enough with 4 leads so it would need to involve that. 

09/22/2021 TA Meeting
=
- Josephine asked us to begin preparing for the design document check. The design document is worth a lot of piints and needs to be long and detailed. 
- Thoughts on pcb design: ask evan about details and for help in making it compact 
- Focus on component selection for the design documents because PCB reviews are also next week
- Asked some questions about the CAD assignment because I lost 4 points, she showed me why I lost them
- Proposal feedback should be given by end of week

09/24/2021 Proposal feedback
=
We got a low grade on our proposal and we have a lot of changed to make before the DDC next week. The 3 of us were a bit confused on the feedback so we met with Josephine again today to get some clarifications. Our block diagram was done completely long, the power data lines weren't clear at all and so we want to begin working on this first. In the meantime, ruthvik is looking into possible parts so we can finalize how our data subsystem will achieve the filteration within 0.5 Hz and 40 Hz. 

09/27/2021 Updated Block diagram
=
Working on the new block diagram now and this is what it looks like:
![Image3](updated_block_diagram.png)

Josephine's feedback is that this looks much better but to make these changes:
- specify the volatage on the power connections
- focus on using the correct types of filters
- what output is the microcontroller sending via bluetooth

I am currently looking into components for the data subsystem. There is a heart rate monitor available on the market that uses a combination of filters to make the signal clearer. I found online that many people use this was an esp32 microcontroller. Need to ask josephine if the heart rate monitor can be used, she said it should be okay. Using an esp32 is something we need to look into as well. The heart rate monitors simplify our pcb a lot because they take in the electrodes signal through an audio jack. 

09/28/2021 Developing Tolerance analysis
=
- Analyzing Dr. Erickson's waveforms today. marked the ST elevation/depression in blue, this is present in 3 leads: v4, v5, v6. NOT present in leads I, II, III. Image of waveform:

![Image20](physionet.png)

- I have been looking for data that provides leads I-III show a heart attack: bound like PR, PP, QT. Asked Dr. Erickson who said to analyze PR segment, long PR segment is common in heart attack patients
- Findings: v4, v5, v6 show ST depression AND long PR segments 
- Findings: I, II show no long PR segments but III does -> sufficient for a tolerance analysis, need to gather data for DDC

09/30/2021 PCB review and DDC
=
The DDC went well and w ehave finalized a lot of parts of our deisgn document. 
I managed to come up with a good tolerance analysis, in order to make up for not having a 12 lead ECG I found that there are various segments of the ECG signal that can be looked at besides just the ST elevation/depression. I sppke to Dr. Erickson and she explained that sometimes if an ST depression sin't visible an extended P interval or a depressed Q wave are signals that somethign could be wrong. I decided to try making these segment calculations and analyzing the lengths according to the bounds of a normal person. This would involve an analysis on all PQRST points like in the picture below. This is an example of an ECG wave and the different points:

![Image4](PQRST.png)


The tolerance analysis for our DD is complete now and looks much better than before. Below is the copy of bounds we will look for in our ML model:

![Image5](ECG_signal_bound.png)

PCB Review: went well, bonyhun suggested using an esp32 development board which support bluetooth. Josephine is also OK with us using this, it's great because it has an in built ADC that we can test with. Ruthvik is working on the PCB design right now and we have our components finalized from the deisgn doc, most importantly the data subsystem is done: AD8232 heart rate monitors and ESP32 dev board. 

10/05/2021 Design Document and Tips for Design Review
= 
Last week we submitted our final DD, all the components are now finalized and we are working on placing the order with our sponsor. Josephine gave us some feedback after the DDC to make RV tables combined, tolerance analysis looks good and the calculations look good just organize the way they are arranged, high level requirements finalized with evan to be the following steps (take measurements, transmit measurements, analyze measurements):

> Take Measurements: The microcontroller should be able to read the user’s heart rhythm by filtering noise using the cutoff frequencies of 0.5 Hz and 40 Hz such that the P, QRS, and T points of the heartbeat are sampled at a rate of at least 100 Hz. 
> Transmit Measurements: The Bluetooth module of the microcontroller can relay recorded data to our phone application within 30 seconds.
> Analyze Measurements: When passed pre-recorded data of an ECG human heartbeat dataset the algorithm can distinguish between the heartbeat of a safe person and one at risk of a heart attack. 

DDC NOTES from Josephine during TA Meeting:
- improve visual aid: pic of battery, pcb, electrodes ... focus on how it would actually look
- we need a physical deisgn image for the design review, where would the board go in the end?

My drafts for the physical design and visual aid:
![Image6](my_visual_aid.jpeg)

10/06/2021 Design Review
=
Finalized the physical design and visual aid to be the following: 
- Visual Aid 
![Image7](final_visual_aid.png)
- Physical Design
![Image8](final_physical_design.png)

10/11/2021 First PCB order
= 
We sent in our pcb for order today, pakhi and I worked on the schematic and ruthvik on the pcb design. For now, we left out the ADC connections, we feel we should first finalzie getting it to work and then add the ADC as a bonus for an even clearer signal. The finalzed schematic for now is:
![Image10](first_schematic.png)

The two heart rate monitors are connected to our ESP32-c3. This is the devboard we chose because it supports BLE (bluetooth low energy): we might have to use a wroom instead, BLE seems easier to get working though. Pakhi and I arent sure about pin connections but we are going to begin testing on a breadboard when the components come. 

10/14/2021 TA Meeting and ML Model progress
=
TA Meeting Notes:
- Began breadboard testing since some components arrived
- PCB issue: wrong dimension for heart rate monitor, need to fix for second order
- ML model questions, can the frontend backend automation be left for the end? trying to clear signals out using python 

ML Model:
I've been developing our model using heart py, they have some predefined functions to clear out the ecg signal so that it looks like this:
![Image11](clear_ecg.png)

I created the code on a jupyter notebook, i'm still trying to use Dr. Erickson's datasets within this to clarify them. The issue rigt now is this packet can't get the point we need -> how do we identify the pqrst points once the signal is clean? I found another python package called neurokit2 that does this but I am having trouble right now with getting our datasets in the correct format (csv).

10/21/2021 TA Meeting and PCB
= 
TA Meeting Notes:
- Sending new pcb for order, Josephine found an issue with ours trying to fix it before tonight's deadline
- Tested the heart rate monitor using arduino, pin connections were easy to figure out and signal is pretty clear -> will be clearer after filtered more with ML model. 
- ESP 32 c3 is giving us some issues, it's working with heart rate monitor but the signal is really unclear, using the RS, TX pins as the inputs and A0 as the signal output. Can't figure out where to connect the second heart rate monitor 

ESP 32-C3 schematic:
![Image12](esp32c3.png)

10/27/2021 TA Meeting and PCB
= 
ML Model thoughts:
- wfdb giving errors, try to use diff computer, package issues on my m1 macbook?
- ecg_clean func: output filters frequencies, modify to set bounds so bandpass ratio = 0.707
- pinpointing Q is easy: peak, before/after is R and S but will this be accurate enough (ask Dr. Erickson)

Accomplished:
- filtered ecg signal, asked pakhi to try installing wfdb
- Managed to find more documentation on neurokit2 uses 
- Reading paper on https://link.springer.com/article/10.3758/s13428-020-01516-y to try and figure out trianing of model
- attempted to integrate heart py and neurokit2, not working, need to figure out cleaning using manual bounds

10/31 IPR and ML Model Progress
=

- Managed to convert the physionet datasets to csv using a wfdb package (steps from physionet)
- Cleared the signals using heart py and created a full jupyter notebook, flow chart is below for ML model
![Image13](flowchart.png)

Still having issues with getting the PQRST points so we can implement our bounds check. Trying to use neurokit2 package but it's having issues with our dataset, need to train it first. 

Wrote my individual progress report about trying to get the ML model to work. Have been working on it for a few weeks now and finally got the datasets to work, heartpy is good for clearing the signal but we need something else so that the analysis of the different segments is as accurate as possible. If this works our entire tolerance analysis would be completed. 

11/04/2021 TA Meeting and ML Model Updates
=
TA Meeting:
- Updates on issues with microcontroller, can get data points but cant see graph
- Breadboard testing needs to continue because we are now having issues with the second heart rate monitor
- pcb order 2 was placed by Dr. Erickson

ML Model Updates:
ML Model is almost fully done, we stopped using heart py. Filtering the signal using bounds, and managed to get neurokit2 working. It takes our csv file and displays a table. Then it prints the signal, zooms in, averages the pqrst points over 10 sec, and places markers on the peaks. Picture is below:
![Image14](analyzed_ecg.png)

Implementing the final boundary checks, changes them a bit now:
- range PR: 0.12-0.22 - taken from https://ecgwaves.com/topic/ecg-normal-p-wave-qrs-complex-st-segment-t-wave-j-point/
- range QRS: 0.08-0.12 - taken from https://ecgwaves.com/topic/ecg-normal-p-wave-qrs-complex-st-segment-t-wave-j-point/
- range QT: 0.35-0.43 - taken from https://www.nottingham.ac.uk/nursing/practice/resources/cardiology/function/normal_duration.php
- range P wave: < 0.12 - taken from https://ecgwaves.com/topic/ecg-normal-p-wave-qrs-complex-st-segment-t-wave-j-point/

sample output using these ranges:
PR wave evaluation:  0.19 True
QRS wave evaluation:  0.11 True
QT wave evaluation:  0.46 False
P wave evaluation:  0.12 True
PQRST feedback: Needs further assesment
ST Elevation evaluation:  1.0025555555555554
ST feedback: elevation/depression present
Patient Evaluation: WARNING!

This is one whole verification complete, ML model is functional and run time within 180 seconds:
CPU times: user 2.13 ms, sys: 550 µs, total: 2.68 ms

11/10/2021 Testing Data Subsystem
= 
Pakhi and I have been breadboard testing a lot using the esp32 c3 and we are still having issues. The second heart rate monitor being connected doesn't allow us to upload the code onto the board. The picture below is of an esp32 wroom, I want to try using this with the following pins:
![Image15](esp32wroom.png)
- A1, A2, A3 
- A0, RX, TX

We ordered an esp32 and it's on the way. Need to see if it can support 2 heart rate monitors, also need to figure out how to make the bluetooth work. BLE isnt enough, doesnt connect to the graphical terminal. 

11/15/2021 Testing Data Subsystem
=
Preparing for the mock demo, esp32 wroom is giving much clearer signal. The issue now is that we need to order a new pcb and get it before the final demo. Pakhi and I have finalized the pin connections, sent these to Ruthvik who is deisgning the new pcb and sending the order to Dr. Erickson before break: 

A0 (left side, 5th pin from top)

Pin: Output (third pin from top)

RX (left side: 14th pin from top)

Pin: LO- (fourth pin from top)

TX (left side: 15th pin from top)

Pin: LO+ (fifth pin from top)

GND (left side: 4th pin from top)

Pin: GND (1st from top)

3V (left side: 2nd pin from top)

Pin: 3.3V (2nd from top)

Vin Pin: Place +ve on board

GND Pin: Place GND on board

A1 (left side: 6th pin from top)

Pin: Output (third pin from top)

27 (right side: 7th pin from top)

Pin: LO- (fourth pin from top)

33 (right side: 8th pin from top)

Pin: LO+ (fifth pin from top)
 
11/17/2021 Mock Demo
= 
Mock Demo/TA Meeting:
- Showed Josephine issues with the esp32c3, how it kept failing.
- Showed her clear signal with the esp32 wroom and how it looks better, picture:
![Image9](wroom_signal.png)

- ML Model: looks good, need to create a frontend for user application
- NEED to figure out bluetooth

After the TA meeting, managed to connect to bluetooth. Found some bluetooth serial code that we can use online and combined it with our original microcontroller code. ECG Signal shows up on phone:

![Image16](bluetooth.png)


Ruthvik sent Dr. Erickson the new pcb to order. Third time's the charm. 

11/29/2021 Final Design
= 
- Dr. Erickson ordered our pcb late, it is now arriving on wednesday instead of today. Josephine and Professor allowed us to extend demo till Friday. 
- Creating the frontend really quickly today. having some issues connecting the jupyter notebook. Ideas to implement: try react for deisgn, server connection, run manual backend connection?  

Anvil:
- can create interface here using components
- need to figure out how to connect to my notebook

The interface looks like this, ended up using an anvil server connection because it works on the phone:
![Image17](interface.png)

We are hoping the pcb comes earlier by tomorrow so we can solder it and troubleshoot. The interface is connected to the jupyter notebook through an anvil server, it isn't autmated yet, will do this if we have time 

12/01/2021 Final PCB
= 
PCB came, we soldered the headers and everything fit on. 
Connections were mostly correct and the signals were detected by the heart rate monitors. 
One microcontroller connection was wrong but we were able to make it work an filter further within the code. Picture of PCB working: 

![Image18](pcb_working.png)

12/03/2021 Final Demo
= 
Today is the final demonstration, preparing on how I should present. Created the handout and included:
- R/V tables
- high level requirements
- I'm presenting the ML model: included a table of the patients and the doctor evaluations. This way Professor and Josephine can see the output of the model for any of the test data sets. The table is like this:
![Image19](patient_eval.png)

Presentation Notes:
- start off by showing them where i load patient files 
- show them patient id, all 12 leads of the ecg shown in the table
- how the markers are placed using the trained model: peaks, onsets, offsets
- how we use the markets for check 1: based on tolerance analysis
- ST elevation/depression for check 2: checking if present
- Show final output. Repeat process for all patients and show how the model changes. 

FINAL SIGNATURE:

I **Pooja Bhagchandani** wrote this notebook and this marks the end of my design process.






















