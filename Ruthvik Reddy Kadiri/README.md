 Lab Notebook for Ruthvik Reddy Kadiri from ECG Shirt (Team 14), ECE 445, Fall 2021.
 =
 Teammates: Pooja Bhagchandani, Pakhi Gupta\
 Sponsor: Dr. Hanna Erickson\
 TA: Melia Josephine 

Note: All contents were committed at once.

Entries:
-

[8/24/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#08-24-2021-first-lecture) \
[8/27/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#08-27-2021-initial-post)\
[8/31/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#08-31-2021-second-lecture-lab-safety-certification)\
[9/7/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#09-07-2021-request-for-approval)\
[9/14/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#09-14-2021-proposal)\
[9/21/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#09-21-2021-design-document)\
[9/23/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#09-23-2021-design-document-second-session)\
[9/27/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#09-27-2021-pcb-review-day)\
[10/4/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#10-04-2021-pcb-design)\
[10/7/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#10-07-2021-pcb-design-second-session)\
[10/14/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#10142021-ta-meeting-testing-and-ml-model)\
[10/20/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#10202021-ordering-parts)\
[10/27/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#10272021-second-pcb)\
[11/1/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#11012021-individual-progress-report-ml-model)\
[11/13/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#11132021-final-test-run)\
[11/17/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#11172021-mock-demo)\
[11/29/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#11292021-frontend-mobile-application)\
[12/1/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#12012021-finishing-touches)\
[12/3/21](https://gitlab.engr.illinois.edu/-/ide/project/pakhig2/ece-445-ecg-shirt/tree/main/-/Ruthvik%20Reddy%20Kadiri/README.md/#12032021-final-demo)


08/24/2021 First Lecture
=
During the first week of class, I was unsure about the kind of project I wanted to take up and went to the lecture where a handfull of ideas were pitched. Out of the ideas pitched,I found Dr. Hannah Erickson's pitch of the ECG Shirt most interesting. The idea involved innovative thinking and was something that could lead to a healthcare product in the future. Seeing this, I am deciding to take this up for creating a proposal. I found some friends in the class (Pakhi and Pooja) who also found the project interesting and wanted to work on it through the webboard.


08/27/2021 Initial Post
=
I had a couple of ideas that I wanted to try for Senior Design but then I decided to the stick with the ECG Shirt. Made contact with Pooja Bhagchandani by replying to Pooja Bhagchandani's webpost and then contacting her.


08/30/2021 Pitching to Dr. Hannah Erikson
=
After getting in touch with Pakhi and Pooja after seeing their webboard post, we are meeting Dr. Hannah Erikson and her team (Meghna Basavaraju & Manisha Naganatanahalli). I decided to get involved with the hardwhile while Pooja and Pakhi looked into software algorithms and modules. Some of the following are the important learnings from the initial research for our pitch to Dr. Erikson that might be relavant later:
- Change in voltage measured by electrodes on the skin is used to measure heart beat.
- Conductors and nano-electrodes within shirt could make for an ideal design.
- A differential amplifier to increase the size of our signal, a low pass filter to eliminate high frequency signals noise (set a cutoff frequency and everything higher than that will be cutoff), a notch filter to filter out specific frequency noises (usually 60 Hz) are design components to consider.
- Since its a T-Shirt, will have to reduce noise from lung movements, however chest gives higher amplitude signals which are essential for accurate readings.
We might want to using adhesive gel electrodes, and we want to provide something that would not have to stick to the skin. 
- Not sticking to the skin is important as we know, but removing the adhesive gel aspect causes issues. In an electrical sense, there is too much noise to accurately measure a heartbeat. The signals coming from these electrodes could be improved using circuit approaches like an LPF or an amplifier.
- Electrodes need to be used in conjunction with a buffer amplifier.

Dr. Erikson agreed to take on the team of me, Pooja and Pakhi among other interested ECE 445 students after listening to our pitch and how much we learnt about the project in such a short span.


08/31/2021 Second Lecture & Lab Safety Certification
=
There were a few more pitch and we discussed different segments of the course and resources available at our disposal. Lab Safety Certification is to be submitted tommowrow and I just confirmed that my previously received certification from other projects did not expire. After class, our team started working on details for the Request for Approval which is due soon. We are trying to make it in time for the early approval deadline to get bonus points but we are unable to make it in time.


09/07/2021 Request for Approval
=
This week, we had to turn in our RFA. We had a decent idea of the problem and the solution we were attempting. However, we had to abstract out systems from the project that we labeled as our different solution components. We are not sure if this is the best breakdown to go with but it's a good estimate. To come up with criteria for success, we decided to look into feauture that are useful and reasonably implementable. Before checking with the ECE department, we estimated our criteria for success to be:
- Fitted design that will decrease background noise and deliver accurate data
- Creation of an app to alert patient as well as first responders
- We would envision that this app has the capability letting the patient decide if first responders should be alerted in case of an emergency or if the patient’s Primary Care Physician should be alerted.
- Patient health information transmitted to their primary care physician.


09/14/2021 Proposal
=
For the project proposal, our team needs to come up with a visual aid to demonstrate the idea which did not take too much effort. The image is as follows: \
![Image](im1.JPG) \
From some research, the final list of high level requirements that I though were reasonable after verifying with the TA's was as:
- The 4 leads will pick up data and transmit it to the application at a rate of 500 Hz in a readable signal wave format that can be analyzed digitally. These leads will be the ones
placed on the: bipolar, unipolar, chest, and augmented. These 4 electrodes should be able to pick up P, QRS, and T waves from the heart rhythm. We will use textile electrodes
rather than gel for more suited augmentation to the user.
- The readings picked up from the 4 lead electrodes will be transmitted into a signal waveform that can be read digitally and analyzed using ML models and programming.
The readable format for our model would be P, QRS, and T waves represented as data points that can be viewed in a 2D graph-like format w x-axis as time and y-axis as
amplitude of the wave in mm which can also be inferred in mV. The primary purpose of the analyzer will be to detect change in ST in comparison to a normal heartbeat.
- Since the readings will be less accurate than an industrial standard 12 lead ECG, we will look for a change in elevation in the ST in the inputted wave signal from the electrodes as an indication of a potential heart attack. This will act as a preemptive warning that something could be wrong, and the user might potentially be at a risk of a heart attack.
This warning alert will be raised by the application which will have the same refresh rate as ECG readings (500 Hz) and will be based on the inputted readings.

We also came up with a high-level block diagram that is a confident estimate containing all the subsytems we need. The block diagram is as follows: \
![Image](im2.JPG)

09/21/2021 Design Document
=
We have the whole week to work on the design document and look into parts that we might need. Most of the neccessary non-technical information was already obtained for the proposal. Most of the effort went into laying out high level requirements and verifications for the requirements based for the subsystems, and laying out a schematic on software. My contribution was towards coming up with verifications for requirements corresponding to the power subsystem which is shown in the table below. \
![Image](im3.JPG)	\
We updated our block diagram with more correct version of what we intended to execute. The diagram is as follows: \
![Image](im4.JPG) \
Following is a list of parts that we think we might need. Following is a list: \
![Image](im8.JPG) \
It took me a lot of time to figure out how to import downloaded components onto the schematic layout page of Kicad. But once I had that figured, I drew our first circuit schematic which is in the picture below. The parts we are using is being determined by my teammate's efforts to meet all the requirements that we came up with. We are having a little trouble understanding how to use the ESP-32 microcontroller chip and will have to put in more work before finalizing part list. Following is the first schematic that was laid out for the ECG Shirt: \
![Image](sch1.PNG)	\
The schematic for the heart beat monitor that we have is as follows: \
![Image](im6.JPG) \
The schematic for the microcontroller that we have is as follows: \
![Image](im7.JPG) \
Hadeel Nasrat Abdullah, Wajid Dawood Alwan, “ECG MONITORING SYSTEM BASED ON MICROCONTROLLER,” is an interesting source of information that I found and am drawing inspiration from. We are unsure about uncertainties and our tolerence analysis and are learning about the different check in place for an ECG system from this resource.


09/23/2021 Design Document Second Session
=
Today, we are spending time finalizing our software needs, especially keeping the healthtech model in mind. We also have to come up with a schedule for ourselfs with goals. While my teammates calculating tolerences and are looking into the datasets we received from our sponsor Dr. Hanna Erikson, I am trying to familiarize myself with different terms used and parameters corresponding to the human heart and the heart beat.
Learnings about software and heartbeat:
- When looking at an ECG signal, we have the P, Q, R, S, and T points of use to us. An ST elevation/depression greater than 1mm qualified as a heart attack, however our machine learning model plans to do more calculations than just that.
- We think that the table below shows all the neccessary factors for our ML model to take into consideration: \
![Image](im9.JPG) \
\
The following is the updated schematic diagram after changing a few parts and adding connections: \
![Image](im5.JPG)	\
\
We also came up with a work plan where we set weekly work portions and split up work among our team. While it is not absolute, this should help us take responsibility for for part of the work. The plan is as follow: \
![Image](im10.JPG) \
While my team is continuing to complete the Design Document, I am getting started on the PCB design. We have decided that I am more hands-on with Kicad and that I will be working on the PCB for the rest of the semester.


09/27/2021 PCB Review Day
=
I am unable to get my PCB reviewed today. I had been facing multiple bugs such as random air wires that make no sense and problems importing footprints. We made changes to some of the parts which is discussed in notebook logs ahead. After hours of debugging with Bonhyun Ku (TA), we cleared out the whole project from the PCB building interface of Kicad but we realized that even when all components were cleared out, there was still an air wire that was floating and could not be deleted. We concluded that we were not using a clean file as we had never seen anything like it before. This was possible as I started the project by adding parts to the schematic of the heartrate monititor instead of starting on a fresh/blank page. This is the only possible reason we could think of and when we used a fresh page, this error did not persist. However, because of this, I am unable to get our PCB reviewed in time today.


10/04/2021 PCB Design
=
We are trying to finish designing and getting the PCB approved in time for this weeks first round of PCB orders being made by the ECE department. Based on the updated schematic diagram, I started working on a fresh PCB design page since I last reviewed the PCB (air wire bug) with TAs. I started by going over the CAD assignment to revise Kicad tricks. However, I was soon stuck with not being able to add filled zones. After spending some trying a lot of things, I got a TA on a Zoom call and we figured that I was stuck on the wrong layer. Restarting the program seemed to do the trick and now layers were switching according to the selection.

A design choice to switch from the ESP-32 chip to the ESP32-02 Dev Board has been made. This was motivated by the ease of connection to the dev board with its built in USB port, voltage regulator and ADC. 


10/07/2021 PCB Design Second Session
=
After all the fixes, the following is the first iteration of the PCB that I came up with. Although we were not able to place the order in time with the ECEB department, our sponsor, Dr. Hanna Erickson was willing to cover any reasonable expenses related to the project. The board costed a lot more than what we expected as our board's dimensions were customized to 127x50mm. This was done in order to integrate the PCB into an armband enclosure and be alligned vertically along the sleeve. Here is the first PCB design with our updated schematic: \
![Image](pcb1.PNG) \
There were numerous errors that I spotted and corrected before sending in the order. Some of them were:
- Using wring wire thickness for power lines. Used 10mm instead of 20mm.
- Missing via
- Mounting holes spaced incorrectly so distance was adjusted.



10/14/2021 TA Meeting, Testing and ML Model
=
Our PCB and components have arrived and we trying testing them out. We instantly realized that the dimension of the heart rate monitor was miscalculated from it not fitting on the PCB. This is a correction to be made in the next PCB that we are ordering. All other components fit well and the lines were working as expected. The ADC however was too fine for us to fit on and we didnt even know how to solder it. Components were breadboard tested and we were still figuring out the usage of the components from their respective documentations. \
We had a TA meeting where we discussed goals and if we were acheiving them. We are considering to leave automating the script of converting data to be fed into the ML model to the very end and focus on getting the subsytems to work as invidual units first. \
While Pooja starting working on the ML model, I was looking into HeartPy and Neurokit libraries of python which are librarires with in built functions for healthtech purposes. Many ECG systems that we found online were built using these modules in a multitude of methods, some more complex than the others.


10/20/2021 Ordering Parts
=
After our first order, we thoroughly tested the parts and decided that we needed more parts ordered in. Spare electrodes and batteries were ordered to avoid curtailing usability in lab. The following is the order placed through the ECE Department. These were parts that were essential in building our setup. \
![Image](bill1.JPG) \
The following order was placed through Dr. Hanna Erikson:
![Image](bill.JPG) 


10/27/2021 Second PCB
=
For the second round of PCB orders, not only did I have to correct the changes to be made to the first PCB, but also had to make adjustments depending on parts ordered subject availablity and convenience of use. Some parts like the microcontroller chip, were replaced even though they were available as we preffered the accessability of the dev board which came with an inbuilt USB port, voltage regulator and ADC. Other than this, measures were taken to decrease noise generated in PCB. Some of these measures were:
- Not running traces under oscillators.
- Not converting from digital to analog after initial conversion.
- Large filled zone.
- Next iteration could ensure that traces are not drawn at right angles to minimize noise generated within the PCB.
Based on the newly ordered parts, the updated schematic is as: \
![Image](schematic.JPG) \
Following are notes for connections that we came up with in the lab for above schematic: \
![Image](pins.JPG) \
The following is the design of our second PCB board: \
![Image](pcb last.JPG) \
A 3D view of the PCB is as follows: \
![Image](PCB Last 3d.JPG) 

Some components were left unconnected to allow external wires to make connection on the board by soldering them on.


11/01/2021 Individual Progress Report & ML Model
= 
Today, I wrote my individual progress report mostly reporting on the PCB construction and design decisions made upto now. Other than this, we have made some progress on the ML model,
We managed to convert the physionet datasets to csv using a wfdb package which we found on physionet. We also cleared the signals using heart py and created a jupyter notebook, and a flow chart for ML model which is as follows: \
![Image](ML.JPG) \
Question for Dr. Erikson after session: "Pinpointing Q is easy: peak, before/after is R and S but will this be accurate enough?"


11/13/2021 Final Test Run
=
- Since the new PCB has not arrived yet, we are presenting with the original design and the original microcontroller. 
- Our intended microcontroller errored out.
- Out of two heartrate monitors, output could only be seen from one of them
- Uploading bluetooth code to the microcontroller was problematic and resulted in struggles with bluetooth functionality

We also demostrated progress on the ML model and discussed the front-end mobile application that we wanted to make to work with the ECG Shirt.

ML model was succesfull, pinpointed the PQRST waves in the pre-recorded data and showed clear ECG signals like below: \
![Image](mockdemo.JPG) 


11/17/2021 Mock Demo
=
Highlights from Mock Demo:
- Demonstrated clear ECG using ESP32 which is as: \
![Image](mockdemo2.JPG) 
- Showed ECG signal on mobile interface. For now it is a graphical interface and can be better formatted and made to look prettier. The mobile interface is as follows: \
![Image](mockdemo3.JPG) 


11/29/2021 Frontend Mobile Application
=
We are using Anvil to create an interface. Still trying to connect our frontend interface to the notebook. The interface after our first iteration looks as shown below. We might keep this as it serves the purpose of the warning message and we do not want to complicate the mobile application subsystem given that none of our teammates are into application design. \
![Image](anvil.JPG) 


12/01/2021 Finishing Touches
=
After fitting everything on the PCB and soldering it up, the system worked as expected to our relief. For our enclosure, we chose to sew a compact pocket on the sleeve of the shirt to house the PCB. While this design can be improved to make sure the PCB is held in a sturdier manner and doesn't poke the user. Following are pictures of the circuit board, view of shirt on the inside. \
![Image](solder.JPG) \
![Image](shirt.JPG) 

Here's a picture of me wearing the shirt! The ECG system was primarily tested on my body and the shirt was used only by me. \
![Image](me.JPG) 


12/03/2021 Final Demo
=
Today is our final demo. For our demo we just hope everything works the way it did in the last test. The batteries on our PCB and the test phone that we have been using are charged. While my teammates do the talking, I will have to wear the T-Shirt and show how different actions cause different amounts of noise along with showing how the PCB comfortably sits on the sleeve. I will be letting my teammates do the talking since I need to avoid generating unneccessary noise in the ECG readings.



***Final Signature:***\
I, **Ruthvik Reddy Kadiri** wrote this notebook and this marks the end of my design process. 12/03/2021


