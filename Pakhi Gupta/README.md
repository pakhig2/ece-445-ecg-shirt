# Pakhi Gupta Lab Notebook

Lab Notebook for Pakhi Gupta from ECG Shirt (Team 14), ECE 445, Fall 2021\
Teammates: Pooja Bhagchandani, Ruthvik Reddy Kadiri\
Sponsor: Dr. Hanna Erickson\
TA: Melia Josephine 

**Entries:**\
[Aug-30-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#aug-30-2021-approaching-dr-hanna-erickson-for-project)\
[Sept-01-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#sept-01-2021-project-expectations-and-understanding-ecg)\
[Sept-07-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#sept-07-2021-rfa)\
[Sept-08-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#sept-08-2021-professor-and-ta-feedback)\
[Sept-14-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#sept-14-2021-parts-selection-and-subsystem-design)\
[Sept-16-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#sept-16-2021-subsystem-requirements-and-block-diagram)\
[Sept-22-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#sept-22-2021-ta-meeting-and-subsystem-design)\
[Sept-25-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#sept-25-2021-understanding-heartbeats-and-proposal-updates)\
[Sept-28-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#sept-28-2021-ddc-and-filtration-system-for-ecg-signals)\
[Oct-04-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#oct-04-2021-finalizing-microcontroller)\
[Oct-06-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#oct-06-2021-finalizing-adc-and-block-diagram)\
[Oct-13-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#oct-13-2021-pin-connections)\
[Oct-15-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#oct-15-2021-schematic-development)\
[Oct-18-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#oct-18-2021-parts-ordering)\
[Oct-22-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#oct-22-2021-updated-schematic)\
[Oct-27-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#oct-27-2021-ml-research-and-ta-meeting)\
[Nov-02-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#nov-02-2021-ble-testing)\
[Nov-04-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#nov-04-2021-ml-model-finished)\
[Nov-09-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#nov-09-2021-bandpass-filter)\
[Nov-11-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#nov-11-2021-microcontroller-change-parts-change)\
[Nov-15-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#nov-15-2021-new-pcb-pin-connections)\
[Nov-17-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#nov-17-2021-mock-demo)\
[Nov-23-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#nov-23-2021-thanksgiving-break-update)\
[Dec-02-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#dec-02-2021-finalizing-project-and-enclosing-pcb)\
[Dec-03-2021](https://gitlab.engr.illinois.edu/pakhig2/ece-445-ecg-shirt/-/tree/main/Pakhi%20Gupta#dec-03-2021-final-demonstration)


## Aug-30-2021 Approaching Dr. Hanna Erickson for Project
My team members and I are interetsted in the ECG Shirt project proposed by Dr. Hanna Erickson. We had our first project meeting with her to discuss potential involvement in this project. We met with the team, Meghna Basavaraju and Manisha Naganatanahalli, along with Dr. Hanna Erickson and pitched our project ideas to her. We presented ideas for three parts of the project. We wanted to delve into hardware, software, and mobile application. I made the slide for mobile application. My idea is to build a mobile application which wirelessly (through Bluetooth or Wifi) collects data from our ECG shirt. I researched and found ways to collect data in realtime from our shirt. I would like the application to display the waves status of the patient in realtime. Some additional features could be an alert system if the patient status changes to heart attack, for example notifying the emergency services. The slide can be seen below. 

![Image1](application_slide.png)

Dr. Hanna Erickson accepted our pitch and has chosen our team (teammates: Ruthvik Reddy Kadiri, Pooja Bhagchandani) to take over her pitched project idea of ECG shirt. 

## Sept-01-2021 Project Expectations and Understanding ECG
Dr. Hanna Erickson has shared documentation on what she expects out of the design and the medical information we will need to understand for successful completion of this project. Moving forward we have to keep these goals in mind as proposed by Dr. Hanna Erickson: 
>-	Low cost: current ECG shirts on the market average around $200 dollars. We want to bring this amount down to reach a broader and underserved patient population
>-	Fitted design that will decrease background noise and deliver accurate data
>-	Machine washable: we want this shirt to be used as an easy use daily wearable that can be cleaned easily and last for a long time
>-	Creation of an app to alert patient as well as first responders
>-	We would envision that this app has the capability letting the patient decide if first responders should be alerted in case of an emergency or if the patient’s Primary Care Physician should be allerted
>-	Patient health information transmitted to their primary care physician 

I am developing a deeper understanding of how an ECG works and the workings of the hearts. 
> The signals, detected by means of metal electrodes attached to the extremities and chest wall, are amplified and recorded by the electrocardiograph. ECG leads (derivations) are configured to display the instantaneous differences in potential between specific pairs of electrodes. In a conventional 12-lead ECG, there are 12 leads calculated using 10 electrodes. Each lead provides a glimpse of the electrical activity of the heart from a particular angle.

For analysing the ECG signals we will need to keep in mind that:
>A wave of depolarization traveling toward a particular electrode on the chest surface will elicit a positive deflection on the ECG. Conversely, a wave of depolarization traveling away from a particular electrode will elicit a negative deflection.

## Sept-07-2021 RFA 
We are preparing for our Request for Approval for our pitched project of ECG shirt today. The problem we are trying to solve with this project is that myocardial infarctions are a leading cause of death in the world and a lot of times these deaths occur due to the fact that the patients were diagnosed too late after the risk of a heart attack had shown up and due to the fact that regular ACG diagnosis can take up to 4+ hours for a result. To avoid this, this ECG shirt should be able to detect these this risk of a heart attack and monitor the patient in real time. We want our product to be better than market competitors such as an Apple Watch since that only has a 1 Lead reading, and ours will be a 12 lead device. We want it in a t-shirt format so that it is comfortable for the user and we want it to be a cost-friendly project so that we can make it widely accessible for the public. 

Some hardware considerations: 
- We need a noise filtering setup: ECG signals will be very noisy due to respiration and other bodily functions, we need to ensure that are signals are clear and amplified 
    - Buffer amplifier, low pass filter, band pass filter 
- Connection to the mobile application 
    - Bluetooth, Wifi, IoT
- Frontend application: alert the user and first responders 
    - React Native 

## Sept-08-2021 Professor and TA Feedback 
We had a discussion with Professor Jonathon Schuh and TA Dean Biskup about the comments we received on our RFA. We have received the feedback that the project seems to be too ambitious for one semester and somehow needs to be simplified. 

Dr. Hanna Erickson helped us come up with a simplified version of the project to adhere to the feedback. Instead of doing a 12-lead ECG Shirt, we will be doing a 3-lead ECG shirt and will make up for our missing leads through a strong ML algorithm which will now help us detect if the user is "at risk" of a heart attack not that if the user is actually having a heart attack or not. 

We decreased our criterian of success and our project has been approved by the ECE 445 course staff. 

## Sept-14-2021 Parts Selection and Subsystem Design 
There are 3 main components of the project: hardware, software (backend), software (frontend). We will need another component for powering the shirt. Therefore, there should be 3 subsystems:
- Power 
- Application (both softwares)
- Data (hardware) 

**Power:** System needs to be portable so the project needs to be battery operated. After research, a buckboost convertor to stabilize voltage seems like a good option. The ADC in data subsystem requires both negative and positive voltage, therefore a switching regulator should work well for that. 

**Data:** We will have electrodes sticking to the user's body and picking up the ECG signals, these signals will be analogue signals, we need an ADC to convert them into digital signals before we feed them into our microcontroller. The microcontroller should have Wifi or Bluetooth functionality to transfer these signals to the application subsystem. 

**Application:** Data from the microcontroller needs to be stored somewhere, an external SD card seems like a good option. We need to come up with a ML model (will use python for it) for analysis of the ECG wave. That analysis can run in the backend of our mobile application (use React Native for it). The mobile application will interact with the user and show their realtime analysis and heartbeat. 

***Parts Selection*** \
Power:
- 3.7 V Battery: HCP502699
- Buckboost Converter: TPS63070 
- Switching Regulator: ADP5072\

Data:
- Electrodes: Textile electrodes (4 electrodes for 3 leads)
-  ADC: AD7768-4
- Microcontroller: AT89C51CC03

## Sept-16-2021 Subsystem Requirements and Block Diagram
Finalized the block diagram for the project. We added the parts information, and detailed all three subsystems in it.

![Image2](blockdiagram1.png)

Apart from the block diagram, we have to finalize on the requirements for each subsystem. We want to keep the requirements achievable yet significant. They should be something that are essential for the working of our project and can be met by us. 

**Power:**
- Microcontroller needs 3.3 V 
- ADC needs ± 2.5 V 

**Data:**
- The refresh rate needs to be at least 500 Hz, so microcontroller should be able to transmit data at that rate 

**Application:**
- The update should be realtime, should update at a rate at least equal to the rate at which the SD card gets data from the microcontroller 

Finalized requirements for all three are:\
**Power:**  
>Power subsystem must supply at least 1400mAh at 3.3V to the microcontroller and
 at +- 2.5V to ADC’s continuously while the electrodes measure the wave and send the wave over
 to our data subsystem.

**Data:**
> The microcontroller in the data subsystem must collect the data from the
  electrodes and supply it to the SD card at a rate of at least 500 Hz.

**Application:**
>The data acquired from the subsystem should be updated on the display at a rate of
 500 Hz + running time of the machine learning algorithm.

## Sept-22-2021 TA Meeting and Subsystem Design
First weekly meeting with the TA. Multiple changes in the proposal needed: 
- High level requirements need to be concise 
- A more detailed visual aid 
- A more detailed block diagram 
- Need to dig in deeper with the tolerance analysis 

Need more concise goals for the project. Can divide into three main functions being performed by the project:
- Taking measurements 
- Transmitting measurements 
- Analysing measurements 

Need to finalize on data transmission method: Bluetooth seems like the best option. There are microcontrollers with Bluetooth modules built in them, and mobile phones already have a Bluetooth module. Additionally, we want to do a mobile application, so Bluetooth is better than Wifi which would have been a good choice for a web application. A reading of 30 seconds seems like a good option, because datasets provided by Dr. Hanna Erickson have a lot of 30 seconds pre-recorded data. 

The power subsystem can be simplified. The buckboost coverter along with the switching regulator is unnecessary. These can be condensed into using just a voltage regulator. We also need to think about the longevity of the device. Maybe an inclusion of a battery charger for the battery would be a good idea.  

## Sept-25-2021 Understanding Heartbeats and Proposal Updates 
Understanding the PQRST waves of the heartbeat a little better. There are ranges for each interval: QRS, RR, PR, QT, R-wave, P-wave, T-wave. We can use these ranges to determine if the heartbeat of the user is normal or not. There is also the ST elevation check. After talking to Dr. Hanna Erickson, because we have downgraded our project from 12-leads to 3-leads, we need to check for ST elevation/depression in our heartbeats, because that will be the primary indicator of if the heartbeat shows a risk of a myocardial infarction or not. 

![Image3](ECG-PQRST.png)

We are updating the high level requirements to this:

- Take Measurements: The  microcontroller should be able to read the user’s heart rhythm by filtering noise using the cutoff frequencies of 0.5 Hz and 40 Hz such that the P, QRS, and T points of the heartbeat are sampled at a rate of at least 100 Hz.
- Transmit Measurements: The Bluetooth module of the microcontroller can relay recorded data to our phone application within 30 seconds.
- Analyze Measurements: When passed pre-recorded data of an ECG human heartbeat dataset the algorithm can distinguish between the heartbeat of a safe person and one at risk of a heart attack.

According to research, the best heartbeats are captured within 0.5 Hz and 40 Hz. We need to implement some sort of a filtration system for our ECG signals. Because we have an upper and a lower limit, we can implement a bandpass filter for our ECG signals which will cut off all incoming data points that do not fall in the range of our desired frequency. 

## Sept-28-2021 DDC and Filtration System for ECG Signals 

Design Document Check feedback:
- Filtration system needs to be strong 
- ESP32 is a popular microcontroller being used by such products, we should look into it 
- Expand our requirements and verification table, be more detailed in it 
- Tolerance analysis still needs more work 

For the filtration system, we need a combination of op-amps such that we can set up a bandpass filter and amplify our signals. 

![Image4](bandpassfilter.jpg)

The AD8232 chip is commonly used for heartbeat ECG signals. 
>The AD8232 is an integrated signal conditioning block for ECG and other biopotential measurement applications. It is designed to extract, amplify, and filter small biopotential signals in the presence of noisy conditions, such as those created by motion or remote electrode placement.

This would work very well with our project. The heart rate monitor frontend can act as our filtration system. The schematic follows the tyoe of amplification and filtration we need. Each heart rate monitor calculates a single lead, therefore we will need two of the heart rate monitors to pick up two leads from the user's body and the third lead will just be calculated in the software since it is just the difference of lead I and lead II. 

![Image5](AD8232schematic.png)

## Oct-04-2021 Finalizing Microcontroller
For the microcontroller, the two main functionalities that are required is the ability to communicate through an SPI peripheral for the ADC and the ability to communicate through Bluetooth. I am in between choosing the ESP32-WROOM-32 or ESP32-C3 as the microcontroller chip. 

**ESP-WROOM-32**\
It has Bluetooth extensions available and an in- built SPI peripheral which satisfied the project’s need; however, communication with this microcontroller is extremely difficult especially programming it to access its Bluetooth module. 

**ESP32-C3**\
This microcontroller gives us USB access to be able to program its SPI peripheral for reading the digital signals from the ADC and its in-built Bluetooth module for transferring the digital signals received to the mobile device. The Bluetooth supports up to 2Mbps transmission rate which satisfies the need of our ML model to receive the data at our sampling rate of 100 Hz. The microcontroller also comes with a built in ADC which is extremely beneficial for testing purposes, although the built in ADC only gives a 12-bit output which is less than our desired output, it will help us in the integration of our external ADC into the circuit.

ESP32-C3 seems like the better option here. It seems like it would be easier integrated for our project and will satisfy more of our needs. 

![Image6](esp32c3schematic.png)

## Oct-06-2021 Finalizing ADC and Block Diagram
The ADC I am choosing is the ADS1192IPBS because it is a 2 channel ADC which is required for our two analog outputs from the two heart rate monitors, so both of our analog signals can be converted by a single ADC. The ADC also has an SPI peripheral built into it which is required to communicate the data from our ADC to our microcontroller. I am also choosing this ADC because it outputs a 16-bit digital signal. We require a 16-bit signal because of its higher precision and because the sample data that has been provided to us by Dr. Hanna Erickson which will be used to train our ML model in the application subsystem is also 16-bit, therefore, to maintain consistency and to accurately use our ML model, a digital output of 16-bits is required.

Since all parts have been chosen, the final block diagram has also been made. It is very different from our initial design and is a much more achievable and realistic project diagram and plan now. 

![Image7](blockdiagram2.png)

## Oct-13-2021 Pin Connections
We missed the first round of PCB orders since our schematic was not fully finalized. However, we are now at the end of finalizing our schematic. 

The connections from the ADC to the microcontroller were confusing. TA has been helping us finalize our connections as well. In office hours today, this is what we finalized: 

![Image8](connections1.jpg)

## Oct-15-2021 Schematic Development
Through more sessions of office hours, TA Bonhyun Ku has been of great help, we have finalized our schematic and all our pin connections. The datasheets of all components (ADC, Heartrate Monitors, Microcontroller) have been the most helpful. 

The TA also suggested looking at the setup guide for the microcontroller and the ADC to figure out the SPI peripheral interface that the two components offer. 

This schematic is using what we will be placing our PCB order: 

![Image9](schematic0.png)

## Oct-18-2021 Parts Ordering
Parts have been ordered through the ECE department and through Dr. Hanna Erickson. 

The parts ordered through the ECE department are those essential for our project. 

![Image10](ECEparts.png)

The parts ordered through Dr. Hanna Erickson will not only help with the project but also help us unit test our parts and test our project according to each subsystem and each functionality. This way, we will be able to better meet the requirements and verifications table and our high level requirements. 

![Image11](externalparts.png)

## Oct-22-2021 Updated Schematic
There have been some changes with the earlier schematic. 
We have improved the earlier schematic to include a testing checkpoint for the project. We made new connections that included connections between the heart rate monitor and the microcontroller to make use of the in-built 12-bit ADC of the microcontroller. This will enable us to test the output of the heart rate monitor without passing them through the external ADC to help with unit testing of the heart rate monitor.

![Image12](connections2.png)
![Image13](connections3.png)

These new connections have translated into the new schematic: 

![Image14](schematic1.png)

## Oct-27-2021 ML Research and TA meeting
We have started work on the ML model. To work on the ML model we need research into what exactly we are looking for and what kind of checks need to be established for the ML model. While my teammate has been working on finding which python libraray to use and which python tools to use to train and develop our ML model, we have also been in talks with Dr. Hanna Erickson and I have been doing research separately and this is all the information we require: 

![Image15](MLresearch1.png)
![Image16](MLresearch2.png)
![Image17](MLresearch3.png)
![Image18](MLresearch4.png)

## Nov-02-2021 BLE Testing
Parts have arrived and breadboard testing for the parts have started. I have been involved in setting up the Bluetooth module of the microcontroller. 

I have written a program to send random numbers to a mobile device through the microcontroller's Bluetooth module. This program enables the microcontroller to look for pairing devices in its Bluetooth signal range, pair with a device if the device asks to get paired with the microcontroller's Bluetooth, and then send random generated numbers to the paired mobile device. 

This will enable us to check how the BLE for microcontroller works. 

![Image19](blecode1.png)
![Image20](blecode2.png)

The BLE works for us. The microcontroller sent the randomized numbers over to the mobile device, which we tracked through the Arduino serial plotter, and the numbers showing up on the mobile device matched up with the numbers on the serial plotter.  

## Nov-04-2021 ML Model Finished 
The ML model is almost completely working now. Instead of heart.py which was the initial python library we wanted to use for ECG analysis, we used Nuerokit2 instead. The ML model was causing problems loading external data files (ones that were not already a part of the Nuerokit library), therefore we had to disect that function and write it from scartch to read a new csv file. 

We have trained the ML model on the datasets provided to us by Dr. Hanna Erickson to better match the needs of our project. Dr. Hanna Erickson is specifically looking for myocardial infarctions and the datasets she provided has several ECG recording of patients facing myocardial infarctions and the symptoms showing up in various leads of the ECG. This will make our model more accurate pertaining to the needs of the project. 

We implemented the functions specific to our project which check for the normal ranges of PQRST wavepoints and ST elevation as listed in the table below: 

![Image21](MLmodelchecktable.png)

It gives us a proper analysis for a normal patient and for a patient experiencing a myocardial infarction. An example analysis:

![Image22](exampleMLanalysis.png)

## Nov-09-2021 Bandpass Filter 
One of the high level requirements that we have is: 
> *Take Measurements:* The microcontroller should be able to read the user’s heart rhythm by filtering noise using the cutoff frequencies of 0.5 Hz and 40 Hz such that the P, QRS, and T points of the heartbeat are sampled at a rate of at least 100 Hz.

Therefore, I will be implementing a bandpass filter for the signals being received by the microcontroller from the heartrate monitor. This bandpass filter will be implemented in the same code where we have implemented the receiving of data for the microcontroller and the transmission of data from the microcontroller using its Bluetooth module. The signals will be filtered after they have been received and before they are transmmitted over through Bluetooth. The signals look like this before filtration: 

![Image23](noisysignal.png)

The filtration was successful. I set the lower limit for the frequency at 0.5 Hz and the upper limit for the frequency at 40 Hz. The ECG signals cleared up very significantly from this band pass filter. These signals are good enough for us to input into our ML model and analyse it. They are also clear enough to view on a graphical viewer and monitor the heartbeat of the user. The new signals look like: 

![Image24](filteredsignal.png)

This has been very good for us because this also verifies one of our requirements in our RV table. 

![Image25](rv1.png)

## Nov-11-2021 Microcontroller Change (Parts Change)
The microcontroller we are using, ESP32-C3-02, is causing problems. It is causing uploading issues when we try to connect two heart rate monitors to it and input two different signals into it to transmit through BLE. We have been looking into the issue and have discovered that the microcontroller only has one channel that is taking input and the other channel is picking up no signal so the code is failing to upload to it. 
Another issue is that this microcontroller supports BLE but not mobile Bluetooth. Our phone is able to connect to the microcontroller and receive values, but it is not able to display the ECG signals on a graphical viewer application on the phone due to BLE restrictions.

Pooja and I are now trying to use different microcontroller, we are trying to revert back to ESP32-WROOM32. 

We switched up our whole breadboard and attached the two heart rate monitors to ESP32-WROOM32. It is supporting the incoming two channel from the heart rate monitors, and both values are being picked up with no problem. ESP32-WROOM32 also has Bluetooth capabilities not BLE, therefore, it is connecting to our mobile and working with a graphical viewer. We will now have to change our microcontroller and order a new PCB for it. 

![Image26](esp32wroom32.png)

## Nov-15-2021 New PCB Pin Connections
Since there has been a change in the microcontroller, the beardboard testing with our new microcontroller ESP32-WROOM32 is now complete. It is compatible with our two new heart rate monitors and its Bluetooth is compatible with the graphical viewer application on our mobile device. We therefore need to finalize the pin connections to design our new PCB and order it. 

These are the finalized connections just between the heart rate monitor, microcontroller and the voltage regulator:

![Image27](connections4.png)

## Nov-17-2021 Mock Demo 
Since the new PCB has not been ordered and is still in the process of being finalized, we are presenting with the original design and the original microcontroller. The microcontroller errored out. 

- The heartbeat signals could only be seen from one heart rate monitor not the other 
- The microcontroller showed consistent issues when trying to upload the Bluetooth code onto it 

- Discussed with the TA how we want to change parts and need to order a new PCB for approval 
- Showed progress on the ML model and discussed that the frontend application needed to be made 

- ML model was succesfull, pinpointed the PQRST waves in the pre-recorded data and showed clear ECG signals like below: 

![Image28](MLheartbeat.png)
![Image29](MLpqrst.png)

## Nov-23-2021 Thanksgiving Break Update  
The new PCB has been ordered however Dr. Hanna Erickson has not yet placed the order so the PCB looks like it will be delivering late. We do not want to demo wihout a PCB, possible solutions:
- Appeal for an extension 
- Have the breadboard model fully functioning and demonstrate with that 

Working on the frontend as well. 
- A simple frontend design 
- A heart image to show purpose and add as design element 
- Want to fully integrate the backend into it; however, seems like it will be a challenge within the time contraints 
- Even without full integration, want the frontend updating if the result in the ML model is updated 
- Have two frontend designs: for "warning" and for "normal"
- Researching how to integrate jupyter notebooks as backends : Anvil seems like a good software to use 

Also look into:
- Converting the ECG signals received on the mobile application into a CSV file to feed into the ML model 

## Dec-02-2021 Finalizing Project and Enclosing PCB
The new PCB has arrived. There were some connection errors on the PCB but those were resolved through software changes. The PCB has been fully soldered and the components have been attached onto it. The PCB is fully functional. 

Now we need to enclose the PCB and integrate it into a T-Shirt. I have chosen a stretchy-soft material, a wool and polyester mix, which would easily enclose the whole PCB and would not scratch on user’s skin. We are attaching the battery on the backside of the PCB to keep the size compact. We need to provide three outlets in our cloth enclosure; two for the wires from the electrodes to the heart rate monitor, and one as a safety outlet to deal with any problems on board with the circuit or the battery. We need to make sure that the end product has no dangling wires and looks like a commercial product. 

![Image30](tshirtout.jpg)
![Image31](tshirtin.jpg)

## Dec-03-2021 Final Demonstration 
I will be presenting the design of the ECG shirt as it fits on the user and the ECG signals as they appear on the mobile device. My talking points are:
- Textile electrodes used 
- Placement of the elctrodes accross the chest and torso
- User comfort has been a key component in making this ECG shirt 
- The integration of the PCB into the t-shirt sleeve 
- The use of the Bluetooth module to send over user's ECG signals over to our mobile device in real time 
- The use of a graphical viewer to display the user's ECG signals 
    - 3000 values of Lead I collected, 5 second pause, 3000 values of Lead II collected, and loop 
- Steps taken and ensured that the user's safety is accounted for, shock prevention using RLD circuit and maintained low voltage throughout the circuit 

The final demo went succesfully today. It was with Professor Arne Filflet and two TAs. The professor and the TAs examined the functionality of the project and our design choices and purpose of the project. The t-shirt was worn by my teammate Ruthvik. 

![Image32](demo1.jpg)
![Image33](demo2.jpg)



***Final Signature:***\
I **Pakhi Gupta** wrote this notebook and this marks the end of my design process. 














